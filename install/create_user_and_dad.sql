-- Example create tablespace, user and dad
CREATE TABLESPACE SCOTT_TBS DATAFILE 'scot_tbs.dat' SIZE 128M AUTOEXTEND ON NEXT 128M;
CREATE USER SCOTT
IDENTIFIED BY Tiger
DEFAULT TABLESPACE SCOTT_TBS;
GRANT CONNECT, RESOURCE TO SCOTT;

BEGIN
  DBMS_EPG.create_dad (
    dad_name => 'SCOTT',
    path     => '/scott/*');

  DBMS_EPG.set_dad_attribute (
    dad_name   => 'SCOTT',
    attr_name  => 'default-page',
    attr_value => 'home');

  DBMS_EPG.set_dad_attribute (
    dad_name   => 'SCOTT',
    attr_name  => 'database-username',
    attr_value => 'SCOTT');

  DBMS_EPG.set_dad_attribute (
    dad_name   => 'SCOTT',
    attr_name  => 'nls-language',
    attr_value => 'slovenian_slovenia.utf8');
  DBMS_EPG.AUTHORIZE_DAD('SCOTT','SCOTT');
    commit;
    END;
/
