create or replace PACKAGE HMAC_AUTH AS
  FUNCTION GENERATE_API_KEY RETURN VARCHAR2;
  PROCEDURE NEW_USER_AUTH (usr_id NUMBER, NEW_AUTH_ID OUT NUMBER, NEW_API_KEY OUT VARCHAR2);
  PROCEDURE HMAC_AUTHORIZATION (i_api_key VARCHAR2, i_api_nonce NUMBER, i_api_sign VARCHAR, R OUT NUMBER);
END HMAC_AUTH;
/

CREATE OR REPLACE PACKAGE BODY HMAC_AUTH AS
  FUNCTION GENERATE_API_KEY
    RETURN VARCHAR2 AS R_API_KEY VARCHAR2(25);
    BEGIN
        R_API_KEY := to_char(sysdate, 'YYMMDDMIHH24')  || translate(dbms_random.string('a', 5), 'AZ', '19')  || REPLACE(to_char(systimestamp, 'SS.FF8'), '.', '');
        RETURN R_API_KEY;
  END GENERATE_API_KEY;

  PROCEDURE NEW_USER_AUTH (usr_id NUMBER, NEW_AUTH_ID OUT NUMBER, NEW_API_KEY OUT VARCHAR2)
      IS
      BEGIN
        INSERT INTO USER_AUTH (API_USER_ID, AUTH_STATUS) VALUES (usr_id, 1) RETURNING AUTH_ID, API_KEY INTO NEW_AUTH_ID, NEW_API_KEY;
        COMMIT;
  END NEW_USER_AUTH;

   PROCEDURE HMAC_AUTHORIZATION (i_api_key  IN   VARCHAR2, i_api_nonce IN NUMBER, i_api_sign IN VARCHAR, R OUT NUMBER)
   IS
    p_auth_id                   NUMBER(10);
    p_api_sign_msg              VARCHAR2(500);
    p_api_nonce                 NUMBER(35);
    p_api_sign                  VARCHAR2(500);
    system_sign                 VARCHAR2(500);

    BEGIN
    SELECT AUTH_ID, API_NONCE INTO p_auth_id, p_api_nonce FROM USER_AUTH WHERE API_KEY = i_api_key AND AUTH_STATUS = 1 AND API_NONCE < i_api_nonce;

    /* User signature */
    p_api_sign_msg := p_auth_id || i_api_key || i_api_nonce;
    p_api_sign := sha256.encrypt(p_api_sign_msg);

    /* system signature */
    system_sign := sha256.encrypt(p_auth_id || i_api_key || i_api_nonce);

    IF p_api_sign = system_sign THEN
        UPDATE USER_AUTH SET REQUESTS_COUNT = REQUESTS_COUNT+1, API_NONCE = i_api_nonce, LAST_REQUEST = SYSDATE WHERE API_KEY = i_api_key AND AUTH_STATUS = 1 AND AUTH_ID = p_auth_id;
        commit;
        R := 1;
    ELSE
        R :=  0;
    END IF;

    EXCEPTION WHEN NO_DATA_FOUND THEN
        R :=  0;
    WHEN OTHERS THEN
       R :=  0;
    END HMAC_AUTHORIZATION;
END HMAC_AUTH;
/
