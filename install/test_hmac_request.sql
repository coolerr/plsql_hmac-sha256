create or replace PROCEDURE TEST_HMAC_REQUEST (api_key  IN   VARCHAR2, api_nonce  IN   NUMBER, api_sign   IN   VARCHAR, api_format  IN   VARCHAR   DEFAULT 'JSON')
IS
  API_AUTH_FAIL          EXCEPTION;
  API_AUTH_RESULT        NUMBER(1);
BEGIN

 HMAC_AUTH.HMAC_AUTHORIZATION(api_key, api_nonce, api_sign, API_AUTH_RESULT);
 IF API_AUTH_RESULT != 1 THEN RAISE API_AUTH_FAIL; END IF;

IF upper(api_format) = 'JSON' THEN
        APEX_JSON.initialize_clob_output;
        APEX_JSON.open_object;
        APEX_JSON.write('success', 1);
        APEX_JSON.write('msg', 'Authorization ok');
        APEX_JSON.close_object;
        htp.p(APEX_JSON.get_clob_output);
        APEX_JSON.free_output;
ELSIF upper(api_format) = 'HTML' THEN
    htp.p('Authorization ok');
ELSIF upper(api_format) = 'SQL' THEN
    dbms_output.put_line('Authorization ok');
END IF;

EXCEPTION WHEN API_AUTH_FAIL THEN
    IF upper(api_format) = 'JSON' THEN
        APEX_JSON.initialize_clob_output;
        APEX_JSON.open_object;
        APEX_JSON.write('success', 0);
        APEX_JSON.write('msg', 'Authorization fail');
        APEX_JSON.close_object;
        htp.p(APEX_JSON.get_clob_output);
        APEX_JSON.free_output;
    ELSIF upper(api_format) = 'HTML' THEN
        htp.p('Authorization fail');
    ELSIF upper(api_format) = 'SQL' THEN
        dbms_output.put_line('Authorization fail exception API_AUTH_FAIL');
    END IF;

WHEN OTHERS THEN
    IF upper(api_format) = 'JSON' THEN
        APEX_JSON.initialize_clob_output;
        APEX_JSON.open_object;
        APEX_JSON.write('success', 0);
        APEX_JSON.write('msg', 'Authorization fail exception OTHERS '||SQLCODE||' '||SQLERRM||'');
        APEX_JSON.close_object;
        htp.p(APEX_JSON.get_clob_output);
        APEX_JSON.free_output;
    ELSIF upper(api_format) = 'HTML' THEN
        htp.p('Authorization fail exception OTHERS '||SQLCODE||' '||SQLERRM||'');
    ELSIF upper(api_format) = 'SQL' THEN
        dbms_output.put_line('Authorization fail exception OTHERS '||SQLCODE||' '||SQLERRM||'');
    END IF;
END TEST_HMAC_REQUEST;
