# plsql_hmac-sha256

API authorization in PL/SQL using Oracle database 11g Express Edition. Clients provide api_key, api_nonce and api_sign which are required as input for HMAC-SHA256 signature request.

[Encode string in HMAC-SHA256 using pl/sql](https://stackoverflow.com/questions/36692999/how-can-i-encode-string-in-hmac-sha256-using-pl-sql)

simple example in PHP:

```
function GetSign($USER_AUTH_ID, $USER_API_KEY)
      {
        $request_nonce = round(microtime(true) * 1000);
        $request_signature = hash( 'sha256', $USER_AUTH_ID . $USER_API_KEY . $request_nonce);

        $signature = array('api_key' => $USER_API_KEY, 'api_nonce' => $request_nonce, 'api_sign' => $request_signature);
        return $signature;
      }

      $request_signature = GetSign($USER_AUTH_ID, $USER_API_KEY);
      $ch = curl_init('localhost:8080/scott/TEST_HMAC_REQUEST');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $request_signature);
      $api_response = curl_exec($ch);
      $api_response_json = json_decode($api_response, true);
      var_dump($request_signature)
      curl_close($ch);
```
